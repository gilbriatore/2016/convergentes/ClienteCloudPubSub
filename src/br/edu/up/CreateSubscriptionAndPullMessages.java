package br.edu.up;

import com.google.cloud.pubsub.Message;
import com.google.cloud.pubsub.PubSub;
import com.google.cloud.pubsub.PubSub.MessageConsumer;
import com.google.cloud.pubsub.PubSub.MessageProcessor;
import com.google.cloud.pubsub.PubSubOptions;
import com.google.cloud.pubsub.Subscription;
import com.google.cloud.pubsub.SubscriptionInfo;

/**
 * A snippet for Google Cloud Pub/Sub showing how to create
 * a Pub/Sub pull subscription and asynchronously pull
 * messages from it.
 */
public class CreateSubscriptionAndPullMessages {

  public static void main(String... args) throws Exception {
    
    try (PubSub pubsub = PubSubOptions.defaultInstance().service()) {
      Subscription subscription = pubsub
          .create(SubscriptionInfo.of("sistemasconvergentes", "MinhaSub"));
      
      MessageProcessor callback = new MessageProcessor() {
        @Override
        
        public void process(Message message) throws Exception {
          
          System.out.printf("Received message \"%s\"%n", message.payloadAsString());
        }
      };
      
      // Create a message consumer and pull messages (for 60
      // seconds)
      try (MessageConsumer consumer = subscription.pullAsync(callback)) {
        
        Thread.sleep(60_000);
        
      }
    }
  }
}